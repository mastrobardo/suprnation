TASK A, (i started with B):   
Will use math js. Tokenization and lexical analyses are a tough matter, better do not reinvent the wheel.

- write a module with basic evaluation
- test it agains matrix example:
    ```
    3 + 2 + 4 T
    +2 T
    −2 T
    sin(30) + cos(20) T
    sin(30 F
    3 + + F
    3+ F
    ```


TASK C:    
Single page application:
    - it will have 1 input field, 'expression'. the input field will be  accessible by the basic controls of the device. 
    - it will have 1 output field, 'result'.
    - it will have a button to save the current expression and it's result. (local storage? Start with a local var for mvp )
    - it will have an area to display saved result. THis area will be  displayed only saved expressions list > 0
    - the expression will be evaluate at each input change.
    - if the expression evaluation fails, a visual feedback is required (color and message)
    - not having design requirements, use Material UI, to speed up.

TASK D 
    - implement a 'rand button'. It will append a random number from https://www.random.org/
        s/?num=1&min=1&max=100&col=1&base=10&format=plain&    
        rnd=new  to the current expression ( so no assumptions on operands )   
    - the input will be frozen until the request will be completed   
    - implement a keyboard layout for numbers [0 ... 9], and the operands [+,-,*,/,sin(),cos()]-> 16 total, 4x4        
    - sin and cos operands will append the empty  operand     
    - to evaluate: in case a portion of text is selected, the special operands will wrapp the txt ( ie: in case 'X' is selected, X will be removed and sin(X) will be appended)     
    - to evaluate: in case a special operand is appended, move the caret to [input.length - 2]     