self.addEventListener('install', e => {
  e.waitUntil(
    caches.open('SuprNation').then(cache => {
      return cache.addAll([
        `/`,
        `/offline.html`,
        `/index.html`,
        `/app.dev.js`
      ])
      .then(() => self.skipWaiting());
    })
  );
});

self.addEventListener('activate', event => {
  event.waitUntil(self.clients.claim());
});

self.addEventListener('fetch', event => {
  event.respondWith(
    caches.open('SuprNation')
      .then(cache => cache.match(event.request, {ignoreSearch: true}))
      .then(response => {
      return response || fetch(event.request);
    })
  );
});