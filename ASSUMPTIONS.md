i end up with react as main tool.    
I know you are on Angular, i know how to use it, but time was a main factor, and i'm faster with React ( also cause  is smtg i was using just yesterday). I apologize, i will continue the angular branch for my own benefit regardless the outcome of the assessment ( i found it a good one, honestly, different from many others i came across lately :+1:)    
i used only functional components for the  project. Probably it would have been better use PureComponent for App and input container, but the elements aren't many enough too slow down rendering phase.    
i used mathjs as main expression evaluator. Lexical Analyses is a though beast ( maybe not for this particular project), but anyway a custom tokenization/parsing system would have required too much time to write. Better not reinvent the wheel if possible.   
For the design part, material-ui. Again, better to use a well tested library. The only bad experience it was with keyboard. I tried to use this package "https://www.npmjs.com/package/react-material-ui-keyboard" but due to outdated dependencies, i was  faster to code a new one.

costants: these i put in each file ( Keyboard, App), for velocity. Will move on webpack's ENV variables if i'll have time ( need some check). Usually, i already have defined them in the container when on a ci.


to RUN: 

```npm i ```
``` npm run dev ```

PLEASE NOTE
MOBILE VIEW ONLY