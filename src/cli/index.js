import {
    // evaluate,
    // TODO: here as reminder,remove 
    compile  
  } from 'mathjs'

// console.log(getResult(process.argv[2])) //cli debug
function getResult (expression){
    let result;
    expression = expression; //prepend for f(-2) case
    try {
        result = evaluate(expression);
    } catch(error) {
        result = 'error'; //one could be tempted to use false. Might use tyescript later this is ugly
    }
    return result;
}

module.export = getResult;
