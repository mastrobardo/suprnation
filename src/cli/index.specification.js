// 3 + 2 + 4 T
// +2 T
// −2 T
// sin(30) + cos(20) T
// sin(30 F
// 3 + + F
// 3+ F

const getResult = require("./index.js");
const describe = require("describe");

describe(
  "the evaluation library is working as expectd, and errors are managed",
  {
    "3 + 2 + 4 T": function() {
      this.expect(getResult("3 + 2 + 4"), 42);
    },
    "+2 and -2": function() {
      this.expect(getResult("+2"), 2);
      this.expect(getResult("-2"), -2);
    },
    "sin(30": function() {
      this.expect(getResult("sin(30"), "error");
    },
    "3 + +": function() {
      this.expect(getResult("3 + +"), "error");
    },
    "3 +": function() {
      this.expect(getResult("3 +"), "error");
    }
  }
);
