import {
  evaluate
  // TODO: here as reminder,remove
  // sqrt, sin, cos:
} from "mathjs";

export function getResult(expression) {
  let result;
  try {
    result = evaluate(expression);
  } catch (error) {
    result = "error"; 
  }
  return result;
}

export function convertUint8ToString(Uint8Arr) {
  let result = +String.fromCharCode.apply(null, Uint8Arr);
  return result;
}
