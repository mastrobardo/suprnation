import '@babel/polyfill'
import React from 'react'
import App from './app/App'
import { render } from 'react-dom'
import './sass/base.scss'
import('./sass/base.scss');


render(
    <App />
  , document.getElementById('root')
)
