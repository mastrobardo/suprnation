export const RNG =
  "https://www.random.org/integers/?num=1&min=1&max=100&col=1&base=10&format=plain&rnd=new"; //TODO: move to env variable

const keys = ["+", "-", "*", "/", "sin()", "cos()"];
const numbers = "1234567890";
export const KEYBOARD_LAYOUT = keys.concat(numbers.split(""))

