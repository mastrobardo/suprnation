import React, { useState } from "react";
import ExpInput from "../components/Input/ExpInput";
import { getResult, convertUint8ToString } from "../utils";
import HistoryView from "../components/History/HistoryView";
import Keyboard from "../components/Keyboard/Keyboard";
import Grid from "@material-ui/core/Grid";
import "../sass/base.scss";
import { RNG, KEYBOARD_LAYOUT } from "../constants";

const isError = input => {
  return input === "error" || (isNaN(input) && !isFinite(input));
};

const App = props => {
  const [expression, setExpression] = useState("");
  const [result, setResult] = useState("");
  const [error, setError] = useState(false);
  const [history, setHistory] = useState([]);

  const calcUpdate = expression => {
    const res = getResult(expression);
    setExpression(expression);
    setResult(res);
    setError(isError(res));
  };

  const handleExpressionChange = event => {
    const input = event.target.value;
    calcUpdate(input);
  };

  const handleSave = event => {
    let x = history.concat();
    let newHistory = { exp: expression, result: result };
    x.unshift(newHistory);
    if (x.length > 5) x.length = 5;
    setHistory(x);
    calcUpdate("");
  };

  const getRandom = async event => {
    const response = await fetch(RNG);
    const reader = response.body.getReader();
    const rawValue = (await reader.read()).value;
    const toAppend = convertUint8ToString(rawValue);
    calcUpdate(expression + toAppend);
  };

  return (
    <div className="container">
      <Grid item xs={12}>
        <Grid container justify="center" spacing={2}>
          <Grid key="header" item>
              <ExpInput
                className="input"
                onChange={handleExpressionChange}
                error={error}
                value={expression}
                label="Input your expression here"
              />
              <ExpInput
                readonly={true}
                className="output"
                value={result}
                label="result"
                className={"unselectable"}
              />
          </Grid>

          <Grid key="board" item>
            <Keyboard
              expression={expression}
              updateExpression={calcUpdate}
              handleSave={handleSave}
              getRandom={getRandom}
              layout={KEYBOARD_LAYOUT}
            />
          </Grid>
        </Grid>
      </Grid>

      <HistoryView history={history} />
    </div>
  );
};

export default App;
