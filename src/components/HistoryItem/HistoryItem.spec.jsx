import React from "react";
import HistoryItem from "./HistoryItem";
import renderer from "react-test-renderer";

it("renders correctly", () => {
  const ele = { exp: "2+2", result: 2 };
  const tree = renderer
    .create(<HistoryItem key={'test'} exp={ele.exp} result={ele.result} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});
