import React from "react";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  textField: {
    margin: '0 auto',
    width: '90%'
  }
}));

const ExpInput = props => {
  const classes = useStyles();
  return (
    <div className={props.className}>
      <TextField
        error={props.error}
        id="expression-input"
        label={props.label}
        value={props.value}
        className={classes.textField}
        margin="normal"
        onChange={props.onChange}
      />
    </div>
  );
};

export default ExpInput;
