import React from "react";
import ExpInput from "./ExpInput";
import renderer from "react-test-renderer";

it("renders correctly", () => {
  const { error, expression, handleExpressionChange } = {
    expression: "2+2",
    error: false,
    handleExpressionChange: () => {}
  };
  const tree = renderer
    .create(
      <ExpInput
        className="input"
        onChange={handleExpressionChange}
        error={error}
        value={expression}
        label="Input your expression here"
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
