import React from "react";
import Keyboard from "./Keyboard";
import renderer from "react-test-renderer";

it("renders correctly", () => {
  const KEYBOARD_LAYOUT = ["+", "-", "*", "/", "sin()", "cos()"];
  const tree = renderer
    .create(
        <Keyboard
        expression={()=>{}}
        updateExpression={()=>{}}
        handleSave={()=>{}}
        getRandom={()=>{}}
        layout={KEYBOARD_LAYOUT}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
