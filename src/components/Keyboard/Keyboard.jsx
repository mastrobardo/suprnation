import React from "react";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";

const Keyboard = props => {
  return (
    <Grid container className="keyboard-container" spacing={2}>
      <Grid item xs={12}>
        <Grid container justify="center" spacing={2}>
        <Grid key='save' item>
        <Button onClick={props.handleSave} variant="contained">
            Save
          </Button>
        </Grid>
        <Grid key='rng' item>
        <Button onClick={props.getRandom} variant="contained">
            Rng
          </Button>
        </Grid>
          {props.layout.map((value, index) => (
            <Grid key={index} item>
              <Button
                onClick={() => {
                  props.updateExpression(props.expression + ' ' + value);
                }}
                variant="contained"
              >
                {value}
              </Button>
            </Grid>
          ))}
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Keyboard;
