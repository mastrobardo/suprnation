import React from "react";
import HistoryItem from "../HistoryItem/HistoryItem"

const HistoryView = props => {
  const h = props.history //|| [];
  return (
    <div>
      {h.map((ele, i) => {
        return <HistoryItem key={i * i} exp={ele.exp} result={ele.result} />
      })}
    </div>
  );
};

export default HistoryView;
