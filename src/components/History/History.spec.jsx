import React from "react";
import HistoryView from "./HistoryView.jsx";
import renderer from "react-test-renderer";

it("renders correctly", () => {
  const tree = renderer
    .create(
      <HistoryView history={[{exp:'1+1', result: 2}]} />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
